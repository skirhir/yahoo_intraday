import pandas as pd
from pandas.io.parsers import read_csv
import urllib2
import logging
import fnmatch
import os
import re
import types
import sys
from collections import Iterable
import pprint


#####################################
#### OPTIONS
#####################################

#Change this from DEBUG to WARNING to reduce log spamming
logging.basicConfig(level=logging.DEBUG,  
                      format='%(asctime)s %(funcName)s %(levelname)s: %(message)s',
                      datefmt='%Y-%m-%d %H:%M:%S')

my_root="./data"

#Granularity is controlled by the amount of history days requested.
#This dict is used for translation between granularity and queried days.
granularity2days= { "1m" : 1,
                    "5m" : 7,
                  }
#####################################
#### Other GLOBAL STUFF
#####################################

sp500_constitutents_uri="http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents.csv"

sp500_ticker="^gspc"

#How many lines will be used when logging a dataset
pd.set_option("display.max_rows",5)
#####################################
#### MISC
#####################################

def _mkdir(newdir):
    """works the way a good mkdir should :)
        - already exists, silently complete
        - regular file in the way, raise an exception
        - parent directory(ies) does not exist, make them as well
    """
    if os.path.isdir(newdir):
        pass
    elif os.path.isfile(newdir):
        raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)
    else:
        head, tail = os.path.split(newdir)
        if head and not os.path.isdir(head):
            _mkdir(head)
        #print "_mkdir %s" % repr(newdir)
        if tail:
            os.mkdir(newdir)


#Printer for debugging purposes:
pp = pprint.PrettyPrinter(indent=4)

###########################################################

#_mkdir(my_root)

  
def get_sp500_tickers():
  """Downloads a list of current sp500 conformant tickers form data.ofkn.org"""
  sp500=read_csv(urllib2.urlopen(sp500_constitutents_uri))
  logging.debug("s&p500 symbols:\n%s" % str(sp500['Symbol']))
  return sp500['Symbol']


def update_yahoo(target, granularity="1m", overwrite=False , my_root=None):
  """Updates the directory with the intraday information for the current market day.
  You have untill the next day market opening to run this succesfully, otherwise data will be missed"""
  
  if my_root is None:
    my_root=globals()['my_root'] 

  #Yahoo financial template, tickers are the same as in http://finance.yahoo.com/echarts
  #if days=1 generates 1 minute data,
  #More than 1 day generates 5 minutes data.
  uri_template="http://chartapi.finance.yahoo.com/instrument/1.0/{ticker}/chartdata;type=quote;range={days}d/csv"

  #Headers for the chartadata.
  #For an example, download: 
  #http://chartapi.finance.yahoo.com/instrument/1.0/%5Egspc/chartdata;type=quote;range=1d/csv
  csv_headers=['Timestamp','close','high','low','open','volume']

  logging.debug("argument: target = %s" % str(target) ) 
  logging.debug("argument: granularity = %s" % granularity) 
  logging.debug("argument: overwrite = %s" % str(overwrite))

  #List of granularity options, currently supported 1minute and 5minutes
  try:
    days=granularity2days[granularity]
  except:
    logging.error( "chosen granularity doesn't exist" )
    raise

  

  #Polytype support: target can be a ticker string or an iterable of ticker strings.
  #If a single ticker is detected, generate a one element array with it for generalization.

  if isinstance(target, types.StringTypes ) or not isinstance(target, Iterable):
    logging.debug("target detected as single ticker")
    target=[ target, ] 
  else:
    logging.debug("target detected as tickers iterable")
    

  for ticker in target:
    logging.debug( "Parsing %s" % ticker )
    uri=uri_template.format(ticker=ticker, days=days)
    logging.debug( "Target uri %s" % uri )

    chartdata=urllib2.urlopen(uri)

    #Discard all header lines
    for line in chartdata:
       #"volume:" is the last line of the header, then the csv starts
       if re.match("volume:",line): break

    #Pandas csv read magic.
    #headers=None + names=csv_headers -> Sets headers manually
    #index_col=Timestamp -> First column was named Timestamp by the headers order, use it as index.
    #parse_dates=True -> Index is of type date
    #date_parser=lambda function -> convert Timestamp from unix time to friendlier datetime object.
    update_ds=read_csv( chartdata, sep=',', header=None,
      names=csv_headers, index_col='Timestamp', parse_dates=True,
      date_parser=lambda x: pd.to_datetime(x,unit='s',utc=True) )

    #If no rows were parsed, something went south.
    if update_ds.shape[0]==0: logging.error("empty ticker %s, on uri %s" % ( ticker, uri ) )

    ####################
    #CSV writing section

    #Create data folder
    _mkdir("{root}/{granularity}/{ticker}".format(root=my_root, granularity=granularity, ticker=ticker))

    #Multiday support: split one dataset in multiple 1 day datasets.
    #by Group by date, then save each on a different file.
    
    #Lambda function: on each datetime object on the index, keep only the date, discard the time.
    #Then Group all rows with the same date.
    #Iterate over the results tuples
    for group_date , group_dataset in update_ds.groupby( lambda x: x.date() ):
      #Generate csv path
      csv_path="{root}/{granularity}/{ticker}/{date}.csv".format( root=my_root,granularity=granularity,ticker=ticker, date=str(group_date))

      logging.debug("processing date: %s" % str(group_date))

      #Overwrite protection
      if os.path.isfile(csv_path) and not overwrite:
        logging.info("target file %s already existed. It was not overwritten" % str(csv_path))
        continue

      #If overwrite protection passed, write the file
      group_dataset.to_csv(csv_path)
      logging.info("target file %s successfully written" %  str(csv_path))
      logging.debug("%s content:\n%s" %  ( str(csv_path), str(group_dataset) ))
      
      



def load_yahoo(target=None, granularity="1m", my_root=None):
  """Traverses my_root directory, returns a dictionary with ticker symbols 
  as keys and a pandas dataset as value. Optional argument: 'target_symbols'.
  If not provided all tickers are returned, otherwise only the tickers inside
  a list are returned.
  Usage example:
  ticker_dict=load_sp500()
  print ticker_dict['GOOG']
  """
  if my_root is None:
    my_root=globals()['my_root'] 

  #For logging purposes
  logging.debug("argument: target = %s" % str(target) ) 
  logging.debug("argument: granularity = %s" % str(granularity) )

  #This dictionary will contain all the results
  ticker_dict={}

  #Polytype support: target can be a ticker string or an iterable of ticker strings.
  #If a single ticker is detected, generate a one element array with it for generaization.
  if target is not None:
    if isinstance(target, types.StringTypes ) or not isinstance(target, Iterable):
      logging.debug("target detected as single ticker")
      target=[ target, ] 
    else:
      logging.debug("target detected as tickers iterable")
  else: 
    logging.debug("No target provided, parsing all available datasets")

  #List of granularity options, currently supported 1minute and 5minutes
  try:
    granularity2days[granularity]
  except:
    logging.error( "chosen granularity doesn't exist" )
    raise

  work_dir="{root}/{granularity}".format(root=my_root, granularity=granularity)

  #generate a walker iterator on the DB root.
  walker=os.walk(work_dir)

  #The DB root contains one folder per ticker

  #this gets a list of folders under the root DB
  #Use folder names to detect tracked tickers
  root,dirs,files=walker.next()
  symbols=dirs

  logging.debug("available symbol folders: %s" % str(symbols))

  #store tree in memory:

  symbol_tree={}
  for symbol in symbols:
    root,dirs,files=walker.next()
    symbol_tree[symbol]={"root":root,"files":files}
  logging.debug( "symbol_tree: %s" % pp.pprint(symbol_tree))


  #Successive walker iterations are on each ticker subfolder
  for symbol in symbol_tree:

    #If target_symbols list was provided, skip the ones outside of the list
    if target is not None:
      if symbol not in target:
        logging.debug("ignoring symbol not in target: %s" % str(symbol))
        continue

    logging.debug("Parsing symbol: %s" % str(symbol))

    #Populate an array with one datasate per each Day CSV found for this ticker
    symbol_datasets=[]
    root=symbol_tree[symbol]["root"]
    for filename in symbol_tree[symbol]["files"]:
      if not fnmatch.fnmatch(filename, '*.csv'):
        logging.debug("ignored file without csv extension: %s" % root + '/'+filename)
        continue

      logging.info("Parsing file: %s" % root + '/'+filename)
        
      symbol_datasets.append (
        read_csv(root + '/'+filename,index_col='Timestamp', parse_dates=True ) )

      logging.debug("%s parsed content:\n%s" %  (  root + '/'+filename , symbol_datasets[-1] ) )
    #merge all the datasets into 1 big dataset, save it on the output dictionary
    ticker_dict[symbol]=pd.concat(symbol_datasets)

  logging.debug("ticker_dictionary:\n%s" % str(ticker_dict))
  return ticker_dict


def update_yahoo_sp500_index(**kwargs):
  """Wrapper function for easily update S&P500 index ticker"""
  update_yahoo(sp500_ticker, **kwargs)

def load_yahoo_sp500_index(**kwargs):
  """Wrapper function for easily update S&P500 index ticker"""
  return load_yahoo(sp500_ticker, **kwargs)[sp500_ticker]

