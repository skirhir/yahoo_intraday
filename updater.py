#!/usr/bin/env python
import sys

import yahoo_intraday 

#Replace with path to the root of your directory structure.
yahoo_intraday.my_root="/tmp/super_root"

#update 5m
yahoo_intraday.update_yahoo_sp500_index(granularity="5m")

#update 1m
yahoo_intraday.update_yahoo_sp500_index(granularity="1m")
